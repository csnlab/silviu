import os
from constants import LFP_PATH, PLOTS_PATH
from neuralynx_io import load_ncs
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from yasa import sw_detect, sw_detection_multi
import glob
from scipy.ndimage import gaussian_filter1d
import seaborn as sns

#session_id = '2012-11-20_s3'
session_id = '2013-4-28_no'
session_folder = os.path.join(LFP_PATH, session_id, 'OFC_LFP')

files = glob.glob(glob.escape(session_folder)+"/*.Ncs")

lfp_data = []
time_stamps = []
time = []

for file in files:
    path = os.path.join(LFP_PATH, session_id, 'OFC_LFP', file)

    ncs = load_ncs(path)  # Load signal data into a dictionary
    ncs['data'][0]  # Access the first sample of data
    lfp_data.append(ncs['data'])
    time_stamps.append(ncs['timestamp'])
    time.append(ncs['time'])

np.testing.assert_array_equal(time_stamps[0], time_stamps[3])
np.testing.assert_array_equal(time[0], time[3])

arr = np.vstack(lfp_data)
# TIME ARRAY MADE TO START AT 0
time_arr = (np.array(time[0])) * 1e-06
start_time = time_arr[0]

FIRST_TS = time_arr[0]



"""The EEG power spectra were computed by a Fast Fourier Transform (FFT) routine 
for 4 sec epochs (0.25 Hz resolution). Detection of individual slow waves was performed 
on the EEG signal after band pass filtering (0.5-4 Hz, stopband edge 
frequencies 0.1-10 Hz) with MATLAB filtfilt function exploiting a Chebyshev Type II 
filter design (MATLAB, The Math Works, Inc., Natick, MA) (Achermann and Borbely, 1997). 
Filter settings were optimized after visual observa-tion to obtain the maximal 
resolution of wave shape, as well as the least intrusion of faster (e.g. spindle)
 activities. Slow waves were detected as negative deflections of the EEG signal between 
 two consecutive positive deflections above the zero-crossing separated by at least 0.1 
 sec (Vyazov-skiy et al., 2007). The first segment of the slow wave (from the first 
 positive peak to the minimal negative peak) and the second segment (from the minimal 
 negative peak to the second positive peak) were detected and their slopes were calculated 
 as mean first derivatives. """


# --- LOAD UP STATES ---

states_file = os.path.join(LFP_PATH, session_id, 'UpDwStt_{}.txt'.format(session_id))
data = pd.read_fwf(states_file, header=None)
cols = [c for c in data.columns if pd.isna(data[c]).sum() == 0]
states = data[cols]
states_cols = ['nUnitInSession',
                'unitThreshol',
                'taskEpisode',
                'StartUpStates',
                'EndUpstates',
               'upStateDuration',
                'spikesPerUpState']
states.columns = states_cols
states['EndUpstates'] = states['EndUpstates'] - 0.1

# --- DETECT SLOW WAVES --------


arr = np.vstack(lfp_data)

sf = int(ncs['header']['SamplingFrequency'])
sw = sw_detect(data=arr,
          sf=sf,
          ch_names=np.arange(arr.shape[0]),
          amp_neg=(50, np.inf), amp_pos=(30, np.inf),
          dur_neg=(0.1, 1.5), dur_pos=(0.1, 1),)

events = sw.summary()
for col in ['Start', 'NegPeak', 'MidCrossing', 'PosPeak', 'End']:
    events[col] = events[col] + FIRST_TS

events.round(2)
mask = sw.get_mask()
data_filt = sw._data_filt
data = sw._data
sw_highlight = data_filt * mask
sw_highlight[sw_highlight == 0] = np.nan



# --- PLOT ONLY THE SLOW WAVES ------------
if False:
    start_time_indx = np.where(np.diff(mask) == 1)[0]
    end_time_indx =  np.where(np.diff(mask) == -1)[0]

    # events['real_start_time'] = time_arr[start_time_indx]
    # events['real_end_time'] = time_arr[end_time_indx]
    # events['start_time_val'] = data_filt[start_time_indx]
    # events['end_time_val'] = data_filt[end_time_indx]

    f, ax = plt.subplots(1, 1, figsize=(16, 4.5))
    for r in data_filt:
        ax.plot(time_arr, r, 'k', label='Bandpass filtered LFP')
    #ax.plot(time_arr, data, 'k', alpha=0.2, label='LFP')
    for r in sw_highlight:
        ax.plot(time_arr, r, 'indianred')

    ax.plot(time_arr[start_time_indx], data_filt[start_time_indx], 'bo', label='Negative peaks')
    ax.plot(events['real_end_time'], events['end_time_val'], 'go', label='Positive peaks')

    # TODO these times fall out of sync!
    # ax.plot(events['NegPeak'], events['ValNegPeak'], 'bo', label='Negative peaks')
    # ax.plot(events['PosPeak'], events['ValPosPeak'], 'go', label='Positive peaks')





# --- FIND IF UP STATES HAVE SLOW WAVES AROUND THEM ----------

states['has_sw'] = False
for i, row in states.iterrows():
    # r1 = (row['StartUpStates'], row['StartUpStates'] - 0.15)
    # r2 = (row['EndUpstates'], row['EndUpstates'] + 0.15)
    #
    # for j, erow in events.iterrows():
    #     e = erow['MidCrossing']
    #     if (e >= r1[0] and e <= r1[1]) or (e >= r2[0] and e <= r2[1]):
    #         print(r1, r2, e)
    #         states.loc[i, 'has_sw'] = True
    #         break

    r1 = row['StartUpStates'] - 0.15, row['EndUpstates'] + 0.15
    sw_pres = mask[np.logical_and(time_arr>= r1[0], time_arr<=r1[1])].sum()

    if sw_pres > 1:
        states.loc[i, 'has_sw'] = True


t_start = FIRST_TS
t_stop = time_arr[-1]

t_start = 2750
t_stop = 2850

indx = np.where(np.logical_and(time_arr>t_start, time_arr<t_stop))


f, ax = plt.subplots(1, 1, figsize=(16, 4.5))
ax.plot(time_arr[indx], data_filt[indx], 'k', label='Bandpass filtered LFP')
ax.plot(time_arr[indx], data[indx], 'k', alpha=0.2, label='LFP')
ax.plot(time_arr[indx], sw_highlight[indx], 'indianred')

start_time_indx = np.where(np.diff(mask) == 1)[0]
start_time_indx = start_time_indx[np.logical_and(time_arr[start_time_indx] >= t_start,
                                                 time_arr[start_time_indx] <= t_stop)]
#ax.plot(time_arr[start_time_indx], data_filt[start_time_indx], 'bo', label='Negative peaks')

for i, row in states.iterrows():
    if row['StartUpStates'] >= t_start and row['EndUpstates'] <= t_stop:
        print(row)
        if row['has_sw']:
            ax.axvspan(row['StartUpStates'], row['EndUpstates'],
                       facecolor=sns.xkcd_rgb['light pastel green'],
                       edgecolor=sns.xkcd_rgb['dark pastel green'],
                       alpha=1,lw=2, zorder=-10)
        else:
            ax.axvspan(row['StartUpStates'], row['EndUpstates'],
                       facecolor=sns.xkcd_rgb['lightish blue'],
                       edgecolor=sns.xkcd_rgb['dark green'],
                       alpha=1, lw=2, zorder=-10)

        #print(row['StartUpStates'], row['EndUpstates'])
ax.set_xlabel('Time (seconds)')
ax.set_ylabel('Amplitude (uV)')
ax.set_ylim([-400, 400])
#ax.xlim([0, times[-1]])
ax.set_title('LFP data (filtered) - session {}'.format(session_id))
ax.legend()
sns.despine()

f.savefig('slow_waves.pdf', dpi=400)


#sw.plot_average(time_before=0.4, time_after=0.8, center="NegPeak");










