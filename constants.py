import glob
import numpy as np

BASE_PATH = '/Users/pietro/data/silviu'
LFP_PATH = '/Users/pietro/data/silviu/OfcLfps'
SPIKES_PATH = '/Users/pietro/data/silviu/tFilesMat'
UPSTATES_PATH = '/Users/pietro/data/silviu/UpStatesCSV'
PLOTS_PATH = '/Users/pietro/data/silviu/plots'


SESSIONS = np.sort([s.split('/')[-1] for s in glob.glob(SPIKES_PATH + '/*')])

