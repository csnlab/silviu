import os
from constants import *
from neuralynx_io import load_ncs
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from yasa import sw_detect
import glob
from scipy.ndimage import gaussian_filter1d
import seaborn as sns
from matplotlib.lines import Line2D
from scipy.stats import zscore


plot_format = 'png'


session_id = '2012-11-20_s3'
session_id = '2013-4-28_no'
#session_id = '2013-5-5_s1'
session_folder = os.path.join(LFP_PATH, session_id, 'OFC_LFP')

use_relative_thr = True



# --- LOAD LFP ----------------

files = glob.glob(glob.escape(session_folder)+"/*.Ncs")

lfp_data = []
time_stamps = []
time = []

for file in files:
    path = os.path.join(LFP_PATH, session_id, 'OFC_LFP', file)

    ncs = load_ncs(path)  # Load signal data into a dictionary
    ncs['data'][0]  # Access the first sample of data
    lfp_data.append(ncs['data'])
    time_stamps.append(ncs['timestamp'])
    time.append(ncs['time'])

np.testing.assert_array_equal(time_stamps[0], time_stamps[3])
np.testing.assert_array_equal(time[0], time[3])

arr = np.vstack(lfp_data)
# TIME ARRAY MADE TO START AT 0
time_arr = (np.array(time[0])) * 1e-06
start_time = time_arr[0]

FIRST_TS = time_arr[0]



# --- LOAD UP STATES ---

states_file = os.path.join(UPSTATES_PATH, '{}_UpStt.csv'.format(session_id))
states = pd.read_csv(states_file)
states['TimeEndUpSt'] = states['TimeEndUpSt'] - 0.1


# --- LOAD SLEEP SCORING ------
scoring_file_name = glob.glob(glob.escape(os.path.join(LFP_PATH, session_id))+'/States*')[0]
data = pd.read_fwf(scoring_file_name, header=None)
cols = [c for c in data.columns if pd.isna(data[c]).sum() == 0]
scoring = data[cols]
scoring_cols = ['t1',
                't2',
                'd',
                'scoring']
scoring.columns = scoring_cols


# --- DETECT SLOW WAVES --------


arr = np.vstack(lfp_data)
arr = arr[0, :].flatten()

sf = int(ncs['header']['SamplingFrequency'])

if use_relative_thr:
    sw = sw_detect(data=zscore(arr),
              sf=sf,
              ch_names=[1],
              amp_neg=(1, None), amp_pos=(1, None), amp_ptp=(1, np.inf),
              dur_neg=(0.1, 1.5), dur_pos=(0.1, 1),)

else:
    sw = sw_detect(data=arr,
              sf=sf,
              ch_names=[1],
              amp_neg=(50, np.inf), amp_pos=(30, np.inf),
              dur_neg=(0.1, 1.5), dur_pos=(0.1, 1),)


events = sw.summary()
for col in ['Start', 'NegPeak', 'MidCrossing', 'PosPeak', 'End']:
    events[col] = events[col] + FIRST_TS

events.round(2)
mask = sw.get_mask()
data_filt = np.squeeze(sw._data_filt)
data = np.squeeze(sw._data)
sw_highlight = data_filt * mask
sw_highlight[sw_highlight == 0] = np.nan



# --- PLOT ONLY THE SLOW WAVES ------------
if False:
    start_time_indx = np.where(np.diff(mask) == 1)[0]
    end_time_indx =  np.where(np.diff(mask) == -1)[0]

    # events['real_start_time'] = time_arr[start_time_indx]
    # events['real_end_time'] = time_arr[end_time_indx]
    # events['start_time_val'] = data_filt[start_time_indx]
    # events['end_time_val'] = data_filt[end_time_indx]

    f, ax = plt.subplots(1, 1, figsize=(16, 4.5))
    ax.plot(time_arr, data_filt, 'k', label='Bandpass filtered LFP')
    ax.plot(time_arr, data, 'k', alpha=0.2, label='LFP')
    ax.plot(time_arr, sw_highlight, 'indianred')

    ax.plot(time_arr[start_time_indx], data_filt[start_time_indx], 'bo', label='Negative peaks')
    ax.plot(events['real_end_time'], events['end_time_val'], 'go', label='Positive peaks')

    # TODO these times fall out of sync!
    # ax.plot(events['NegPeak'], events['ValNegPeak'], 'bo', label='Negative peaks')
    # ax.plot(events['PosPeak'], events['ValPosPeak'], 'go', label='Positive peaks')





# --- FIND IF UP STATES HAVE SLOW WAVES AROUND THEM ----------

states['has_sw'] = False
for i, row in states.iterrows():
    # r1 = (row['StartUpStates'], row['StartUpStates'] - 0.15)
    # r2 = (row['EndUpstates'], row['EndUpstates'] + 0.15)
    #
    # for j, erow in events.iterrows():
    #     e = erow['MidCrossing']
    #     if (e >= r1[0] and e <= r1[1]) or (e >= r2[0] and e <= r2[1]):
    #         print(r1, r2, e)
    #         states.loc[i, 'has_sw'] = True
    #         break

    r1 = row['TimeStartUpSt'] - 0.15, row['TimeEndUpSt'] + 0.15
    sw_pres = mask[np.logical_and(time_arr>= r1[0], time_arr<=r1[1])].sum()

    if sw_pres > 1:
        states.loc[i, 'has_sw'] = True


scoring_palette = {'AWAKE' : sns.xkcd_rgb['blue'],
                   'SWS' : sns.xkcd_rgb['red'],
                   'REM' : sns.xkcd_rgb['green'],
                   'UNKNOWN' : sns.xkcd_rgb['grey']}

t_start = FIRST_TS
t_stop = time_arr[-1]


views = [(2000, 3000),
         (3000, 4500),
         (5000, 6500),
         (7000, 8500),
         (8000, 8100)]

for vv, (t_start, t_stop) in enumerate(views):

    indx = np.where(np.logical_and(time_arr>t_start, time_arr<t_stop))

    import matplotlib as mpl

    mpl.rcParams['agg.path.chunksize'] = 10000

    f, ax = plt.subplots(1, 1, figsize=(16, 4.5))


    ax.plot(time_arr[indx], data_filt[indx], 'k', label='Bandpass filtered LFP')
    ax.plot(time_arr[indx], data[indx], 'k', alpha=0.2, label='LFP')
    ax.plot(time_arr[indx], sw_highlight[indx], 'indianred')

    start_time_indx = np.where(np.diff(mask) == 1)[0]
    start_time_indx = start_time_indx[np.logical_and(time_arr[start_time_indx] >= t_start,
                                                     time_arr[start_time_indx] <= t_stop)]
    #ax.plot(time_arr[start_time_indx], data_filt[start_time_indx], 'bo', label='Negative peaks')

    for i, row in states.iterrows():
        if row['TimeStartUpSt'] >= t_start and row['TimeEndUpSt'] <= t_stop:
            print(row)
            if row['has_sw']:
                ax.axvspan(row['TimeStartUpSt'], row['TimeEndUpSt'],
                           facecolor=sns.xkcd_rgb['light orange'],
                           edgecolor=sns.xkcd_rgb['dark orange'],
                           alpha=0.5,lw=0, zorder=-10)
                #ax.axvline(row['StartUpStates'], lw=1, c=sns.xkcd_rgb['dark orange'], zorder=-10)
            else:
                ax.axvspan(row['TimeStartUpSt'], row['TimeEndUpSt'],
                           facecolor=sns.xkcd_rgb['light yellow'],
                           edgecolor=sns.xkcd_rgb['dark yellow'],
                           alpha=0.5, lw=0, zorder=-10)
                #ax.axvline(row['StartUpStates'], lw=1, c=sns.xkcd_rgb['dark yellow'], zorder=-10)

            #print(row['StartUpStates'], row['EndUpstates'])

    for i, p in scoring.iterrows():
        where = ax.get_ylim()[0]+(ax.get_ylim()[1] - ax.get_ylim()[0])/10
        ax.plot((p['t1'], p['t2']), (where, where), c=scoring_palette[p['scoring']], lw=5)

    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel('Amplitude (uV)')
    #ax.set_ylim([-400, 400])
    ax.set_xlim([t_start, t_stop])

    #ax.xlim([0, times[-1]])
    #ax.set_title('LFP data (filtered) - session {}'.format(session_id))
    ax.legend().remove()
    sns.despine()

    f.savefig(os.path.join(PLOTS_PATH, '{}_slow_waves_{}.{}'.format(session_id, vv, plot_format)), dpi=400)



# --- PERCENTAGE RETAINED UP STATES -------

f, ax = plt.subplots(1, 1)
sns.countplot(data=states, x='has_sw')
ax.set_xticklabels(['With SW', 'Without SW'])
ax.set_xlabel('')
ax.set_ylabel('# of UP states')
sns.despine()
plt.tight_layout()
f.savefig(os.path.join(PLOTS_PATH, 'ratio_up_states_with_sw.{}'.format(plot_format)), dpi=400)



# --- LEGEND 1
f, ax = plt.subplots(1, 1, figsize=(2, 2))

custom_lines = [Line2D([0], [0], color=scoring_palette['SWS'], lw=5),
                Line2D([0], [0], color=scoring_palette['REM'], lw=5),
                Line2D([0], [0], color=scoring_palette['AWAKE'], lw=5),
                Line2D([0], [0], color=scoring_palette['UNKNOWN'], lw=5)]
texts = ['SWS', 'Rem', 'Awake', 'Unknown']

ax.legend(custom_lines, texts, loc='center', frameon=False)
ax.axis('off')
f.savefig(os.path.join(PLOTS_PATH, 'leg1.{}'.format(plot_format)), dpi=400)


# --- LEGEND 2
f, ax = plt.subplots(1, 1, figsize=(2, 2))

custom_lines = [Line2D([0], [0], color='k', lw=1),
                Line2D([0], [0], color='grey', lw=1),
                Line2D([0], [0], color='indianred', lw=1)]

texts = ['Filtered LFP', 'LFP', 'Detected slow wave']

ax.legend(custom_lines, texts, loc='center', frameon=False)
ax.axis('off')
f.savefig(os.path.join(PLOTS_PATH, 'leg2.{}'.format(plot_format)), dpi=400)


# --- LEGEND 3
f, ax = plt.subplots(1, 1, figsize=(2, 2))

custom_lines = [Line2D([0], [0], color=sns.xkcd_rgb['light orange'], alpha=0.5, lw=8),
                Line2D([0], [0], color=sns.xkcd_rgb['light yellow'], alpha=0.5, lw=8)]

texts = ['UP states with SW', 'UP states without SW']

ax.legend(custom_lines, texts, loc='center', frameon=False)
ax.axis('off')
f.savefig(os.path.join(PLOTS_PATH, 'leg3.{}'.format(plot_format)), dpi=400)




#f = plt.figure()

ax = sw.plot_average(time_before=0.4, time_after=0.8, center="NegPeak",
                ci='sd', figsize=(3, 3))
ax.legend().remove()
plt.tight_layout()
sns.despine()

# f.add_axes(ax)
# f.savefig(os.path.join(PLOTS_PATH, 'sw_average.{}'.format(plot_format)), dpi=400)
#


"""
- which channel to pick
- look for slow waves within the bound?
- separate sleep?
- up states terminate a while before the end of the recording
- validate in one session or in all sessions?

"""









