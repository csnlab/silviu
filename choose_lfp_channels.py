import os
from constants import *
from neuralynx_io import load_ncs
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from yasa import sw_detect
import glob
from scipy.ndimage import gaussian_filter1d
import seaborn as sns
from matplotlib.lines import Line2D
from scipy.stats import zscore


plot_format = 'png'


use_relative_thr = True

plot_folder = os.path.join(PLOTS_PATH, 'lfp_channel_selection')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

for session_id in SESSIONS:
    print(session_id)

    # --- LOAD LFP ----------------
    session_folder = os.path.join(LFP_PATH, session_id, 'OFC_LFP')
    files = glob.glob(glob.escape(session_folder)+"/*.Ncs")

    lfp_data = []
    time_stamps = []
    time = []

    for file in files:
        path = os.path.join(LFP_PATH, session_id, 'OFC_LFP', file)
        ncs = load_ncs(path)  # Load signal data into a dictionary
        ncs['data'][0]  # Access the first sample of data
        lfp_data.append(ncs['data'])
        time_stamps.append(ncs['timestamp'])
        time.append(ncs['time'])

    #np.testing.assert_array_equal(time_stamps[0], time_stamps[3])
    #np.testing.assert_array_equal(time[0], time[3])

    arr = np.vstack(lfp_data)
    # TIME ARRAY MADE TO START AT 0
    time_arr = (np.array(time[0])) * 1e-06
    start_time = time_arr[0]

    FIRST_TS = time_arr[0]

    from scipy.signal import welch




    views = [(3000, 3020),
             (4000, 4020),
             (5000, 5020),
             (6000, 6020),
             (7000, 7020)]

    for vv, (t_start, t_stop) in enumerate(views):

        indx = np.where(np.logical_and(time_arr>t_start, time_arr<t_stop))

        import matplotlib as mpl

        mpl.rcParams['agg.path.chunksize'] = 10000

        f, ax = plt.subplots(1, 1, figsize=(6, 4))

        for i, (file, chan) in enumerate(zip(files, arr)):
            print(chan[indx])
            ax.plot(time_arr[indx][::20], chan[indx][::20]+i*500, label=file.split('/')[-1])
        ax.legend(frameon=False)
        sns.despine()
        plt.tight_layout()
        f.savefig(os.path.join(plot_folder, '{}_lfp_{}.{}'.format(session_id, vv, plot_format)), dpi=400)

