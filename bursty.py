import os
import glob
from constants import *
from neuralynx_io import load_ncs
import matplotlib.pyplot as plt
import pandas as pd
from loadmat import *
import neo
import quantities as pq
import elephant
import seaborn as sns
from sklearn.preprocessing import minmax_scale
from sklearn.decomposition import PCA
from scipy.stats import kstest
import matplotlib

#session_id = '2013-4-28_no'
#unit_id = 'Sc7_19'

matplotlib.rcParams['pdf.fonttype'] = 42
plot_format = 'pdf'


isi_histograms = []
session_ids_all = []
unit_ids_all = []

bin_size = 0.005
max_isi = 0.2
min_pairs = 1

isi_bins = np.arange(0, max_isi + bin_size, bin_size)

for session_id in SESSIONS:


    unit_ids = np.sort([s.split('/')[-1].split('.')[0] for s in
                        glob.glob(SPIKES_PATH+'/{}/*'.format(session_id))])

    for unit_id in unit_ids:

        path = os.path.join(SPIKES_PATH, session_id, '{}.mat'.format(unit_id))
        file = loadmat(open(path, 'rb'))

        spikes = file['TSI']

        st = neo.SpikeTrain(spikes * pq.s, t_start=spikes[0] * pq.s,
                            t_stop=spikes[-1] * pq.s)


        isi = elephant.statistics.isi(st)

        #sns.kdeplot(isi.__array__())

        hist = np.histogram(a=isi, bins=isi_bins, density=True)

        isi_histograms.append(hist[0])
        unit_ids_all.append(unit_id)
        session_ids_all.append(session_id)


unique_unit_id = ['{}_{}'.format(s, u) for s, u in zip(session_ids_all, unit_ids_all)]
stack = np.vstack(isi_histograms)
stack = np.vstack([minmax_scale(r) for r in stack])

indx_max = np.argmax(stack, axis=1)
indx = np.argsort(indx_max)

sorted_stack = stack[indx, :]
sorted_unique_unit_id = np.array(unique_unit_id)[indx]

ds = pd.DataFrame(index=sorted_unique_unit_id, columns=['isihist'])
ds['isihist'] = [r for r in sorted_stack]

# --- PLOT ALL CELLS SORTED ---
f, ax = plt.subplots(1, 1, figsize=[2, 5])
ax.imshow(sorted_stack, aspect='auto', extent=(0, max_isi*1000, sorted_stack.shape[0]+1, 1))
ax.set_xlabel('Time [ms]')
ax.set_ylabel('Neurons')
sns.despine()
plt.tight_layout()

f.savefig(os.path.join(PLOTS_PATH, 'isi_all_cells.{}'.format(plot_format)), dpi=400)

# --- PLOT INDIVIDUAL EXAMPLES ---

for single_cell_idx in [100, 150, 200, 300, 350, 400, -1]:
    f, ax = plt.subplots(1, 1, figsize=[2, 1.5])
    plt.bar(x=(isi_bins+bin_size/2)[:-1]*1000, height=sorted_stack[single_cell_idx, :],
            width=bin_size*1000,  color=sns.xkcd_rgb['pastel orange'])
    ax.set_xlim([0, max_isi*1000])
    ax.set_xlabel('Time [ms]')
    ax.set_ylabel('ISI prob.')
    sns.despine()
    plt.tight_layout()

    f.savefig(os.path.join(PLOTS_PATH, 'isi_single_cell_{}.{}'.format(single_cell_idx, plot_format)), dpi=400)



# --- LOAD CONTRIBUTOR PAIRS ---


# Contributors
df = pd.read_excel(open(os.path.join(BASE_PATH, 'contributorsNonContributors.xlsx'), 'rb'),
                   sheet_name='contributors')
df['cell1'] = ['{}_{}'.format(s, u) for s, u in zip(df['session'], df['cell1inPair'])]
df['cell2'] = ['{}_{}'.format(s, u) for s, u in zip(df['session'], df['cell2inPair'])]

contributors = np.hstack([df['cell1'].values, df['cell2'].values])
valc = pd.value_counts(contributors)
contributors = np.array(valc[valc >= min_pairs].index)


# non-contributors
df = pd.read_excel(open(os.path.join(BASE_PATH, 'contributorsNonContributors.xlsx'), 'rb'),
                   sheet_name='nonContributors')
df['cell1'] = ['{}_{}'.format(s, u) for s, u in zip(df['session'], df['pair1'])]
df['cell2'] = ['{}_{}'.format(s, u) for s, u in zip(df['session'], df['pair2'])]

non_contributors = np.hstack([df['cell1'].values, df['cell2'].values])
valc = pd.value_counts(non_contributors)
non_contributors = np.array(valc[valc >= min_pairs].index)


# TAKE ONLY PURE CONTRIBUTORS: REMOVE OVERLAP BETWEEN SETS
intersection = set(contributors).intersection(set(non_contributors))
contributors = np.array(list(set(contributors).difference(set(intersection))))
non_contributors = np.array(list(set(non_contributors).difference(set(intersection))))


# other cells
othercells = np.array([u for u in unique_unit_id if ~np.isin(u, contributors) and ~np.isin(u, non_contributors)])

assert len(othercells) + len(contributors) + len(non_contributors) == len(unique_unit_id)

# SORT
cont_stack = np.vstack(ds.loc[contributors]['isihist'])
indx_max = np.argmax(cont_stack, axis=1)
indx = np.argsort(indx_max)
sort_cont_stack = cont_stack[indx, :]
contributors = contributors[indx]
nc = len(contributors)

noncont_stack = np.vstack(ds.loc[non_contributors]['isihist'])
indx_max = np.argmax(noncont_stack, axis=1)
indx = np.argsort(indx_max)
sort_non_cont_stack = noncont_stack[indx, :]
non_contributors = non_contributors[indx]
nnc = len(non_contributors)

# unclassified cells
othercells_stack = np.vstack(ds.loc[othercells]['isihist'])
indx_max = np.argmax(othercells_stack, axis=1)
indx = np.argsort(indx_max)
sort_othercells_stack = othercells_stack[indx, :]
othercells = othercells[indx]
noc = len(othercells)


f, ax = plt.subplots(1, 2, figsize=[4, 5])
ax[0].imshow(sort_cont_stack, aspect='auto', extent=(0, max_isi*1000, sort_cont_stack.shape[0]+1, 1))
ax[1].imshow(sort_non_cont_stack, aspect='auto', extent=(0, max_isi * 1000, sort_non_cont_stack.shape[0] + 1, 1))
ax[0].set_xlabel('Time [ms]')
ax[1].set_xlabel('Time [ms]')
ax[0].set_ylabel('Contributor neurons')
ax[1].set_ylabel('Non-contributor neurons')

sns.despine()
plt.tight_layout()

f.savefig(os.path.join(PLOTS_PATH, 'isi_cont_vs_noncont.{}'.format(plot_format)), dpi=400)



# --- PCAAAAAA -----------------------------------------------------------------


pca = PCA(n_components=2)
X = np.vstack([cont_stack, sort_non_cont_stack, sort_othercells_stack])

pc = pca.fit_transform(X)


alpha=0.6
f, ax = plt.subplots(1, 1, figsize=[3, 3])
ax.scatter(pc[0:nc, 0], pc[0:nc, 1], c='red', s=20, alpha=alpha,
           label='Contributors', lw=0)
ax.scatter(pc[nc:nc+nnc, 0], pc[nc:nc+nnc, 1], c='blue', s=20, alpha=alpha,
           label='Non-contributors', lw=0)
ax.scatter(pc[nc+nnc:, 0], pc[nc+nnc:, 1], c='grey', s=10, alpha=alpha,
           label='Other', lw=0, zorder=-10)
ax.set_xlabel('PC 1')
ax.set_ylabel('PC 2')
ax.legend(loc='upper left', frameon=False)
sns.despine()
plt.tight_layout()
f.savefig(os.path.join(PLOTS_PATH, 'PC_scatter.{}'.format(plot_format)), dpi=400)


bins = np.linspace(pc[:, 0].min(), pc[:, 0].max(), 15)
f, ax = plt.subplots(1, 1, figsize=[3, 2])
ax.hist(pc[0:nc, 0], bins=bins, color='red', label='Contributors',
        density=True, alpha=alpha)
ax.hist(pc[nc:nc+nnc, 0], bins=bins, color='blue', label='Non-contributors',
        density=True, alpha=alpha)
# ax.hist(pc[nc+nnc:, 0], bins=bins, color='grey', label='Non-contributors',
#         density=True, alpha=0.6)
#ax.get_yaxis().set_visible(False)
ax.legend(loc='upper right', frameon=False, fontsize=8)
ax.set_xlabel('PC 1')
ax.set_ylabel('Density')
sns.despine()
plt.tight_layout()
f.savefig(os.path.join(PLOTS_PATH, 'PC1_hist.{}'.format(plot_format)), dpi=400)



bins = np.linspace(pc[:, 1].min(), pc[:, 1].max(), 15)
f, ax = plt.subplots(1, 1, figsize=[3, 2])
ax.hist(pc[0:nc, 1], bins=bins, color='red', label='Contributors',
        density=True, alpha=alpha)
ax.hist(pc[nc:nc+nnc, 1], bins=bins, color='blue', label='Non-contributors',
        density=True, alpha=alpha)
# ax.hist(pc[nc+nnc:, 0], bins=bins, color='grey', label='Non-contributors',
#         density=True, alpha=0.6)
#ax.get_yaxis().set_visible(False)
ax.set_xlabel('PC 2')
ax.set_ylabel('Density')
sns.despine()
plt.tight_layout()
f.savefig(os.path.join(PLOTS_PATH, 'PC2_hist.{}'.format(plot_format)), dpi=400)



dx = pd.DataFrame(columns=['PC1', 'PC2', 'type'])
dx['PC1'] = pc[:, 0]
dx['PC2'] = pc[:, 1]
dx['type'] = np.hstack([np.repeat('c', nc), np.repeat('nc', nnc), np.repeat('oc', noc)])

stat, p1 = kstest(pc[0:nc, 0], pc[nc:nc+nnc, 0])
print('KS test on PC 1 : {}'.format(p1))

stat, p2 = kstest(pc[0:nc, 1], pc[nc:nc+nnc, 1])
print('KS test on PC 2 : {}'.format(p2))
