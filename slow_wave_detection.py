import os
from constants import *
from neuralynx_io import load_ncs
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from yasa import sw_detect
import glob
from scipy.ndimage import gaussian_filter1d
import seaborn as sns
from matplotlib.lines import Line2D
from scipy.stats import zscore
from scipy.signal import welch


plot_format = 'png'


use_relative_thr = True

plot_folder = os.path.join(PLOTS_PATH, 'sw_detection')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

for session_id in [SESSIONS[0], SESSIONS[10], SESSIONS[20]]:
    print(session_id)

    # --- LOAD LFP ----------------
    session_folder = os.path.join(LFP_PATH, session_id, 'OFC_LFP')
    files = glob.glob(glob.escape(session_folder)+"/*.Ncs")

    lfp_data = []
    time_stamps = []
    time = []

    for file in files[0:1]:
        path = os.path.join(LFP_PATH, session_id, 'OFC_LFP', file)
        ncs = load_ncs(path)  # Load signal data into a dictionary
        ncs['data'][0]  # Access the first sample of data
        lfp_data.append(ncs['data'])
        time_stamps.append(ncs['timestamp'])
        time.append(ncs['time'])

    channel_name = file.split('/')[-1]
    #np.testing.assert_array_equal(time_stamps[0], time_stamps[3])
    #np.testing.assert_array_equal(time[0], time[3])

    arr = np.vstack(lfp_data)
    # TIME ARRAY MADE TO START AT 0
    time_arr = (np.array(time[0])) * 1e-06
    start_time = time_arr[0]

    FIRST_TS = time_arr[0]

    # --- LOAD UP STATES ---

    states_file = os.path.join(UPSTATES_PATH, '{}_UpStt.csv'.format(session_id))
    states = pd.read_csv(states_file)
    states['TimeEndUpSt'] = states['TimeEndUpSt'] - 0.1


    # --- LOAD SLEEP SCORING ------
    scoring_file_name = glob.glob(glob.escape(os.path.join(LFP_PATH, session_id))+'/States*')[0]
    data = pd.read_fwf(scoring_file_name, header=None)
    cols = [c for c in data.columns if pd.isna(data[c]).sum() == 0]
    scoring = data[cols]
    scoring_cols = ['t1',
                    't2',
                    'd',
                    'scoring']
    scoring.columns = scoring_cols
    scoring['t1'] = scoring['t1']+FIRST_TS
    scoring['t2'] = scoring['t2']+FIRST_TS
    scoring = scoring.sort_values(by='t1')


    # --- DETECT SLOW WAVES --------

    arr = np.vstack(lfp_data)
    arr = arr[0, :].flatten()

    sf = int(ncs['header']['SamplingFrequency'])

    if use_relative_thr:
        sw = sw_detect(data=zscore(arr),
                  sf=sf,
                  ch_names=[1],
                  amp_neg=(0.5, None), amp_pos=(0.5, None), amp_ptp=(1, np.inf),
                  dur_neg=(0.1, 1.5), dur_pos=(0.1, 1),)

    else:
        sw = sw_detect(data=arr,
                  sf=sf,
                  ch_names=[1],
                  amp_neg=(50, np.inf), amp_pos=(30, np.inf),
                  dur_neg=(0.1, 1.5), dur_pos=(0.1, 1),)


    events = sw.summary()
    for col in ['Start', 'NegPeak', 'MidCrossing', 'PosPeak', 'End']:
        events[col] = events[col] + FIRST_TS

    events.round(2)
    mask = sw.get_mask()
    data_filt = np.squeeze(sw._data_filt)
    data = np.squeeze(sw._data)
    sw_highlight = data_filt * mask
    sw_highlight[sw_highlight == 0] = np.nan


    # --- FIND IF UP STATES HAVE SLOW WAVES IN THEM ----------

    states['has_sw'] = False
    for i, row in states.iterrows():
        # r1 = (row['StartUpStates'], row['StartUpStates'] - 0.15)
        # r2 = (row['EndUpstates'], row['EndUpstates'] + 0.15)
        #
        # for j, erow in events.iterrows():
        #     e = erow['MidCrossing']
        #     if (e >= r1[0] and e <= r1[1]) or (e >= r2[0] and e <= r2[1]):
        #         print(r1, r2, e)
        #         states.loc[i, 'has_sw'] = True
        #         break

        r1 = row['TimeStartUpSt'] - 0.15, row['TimeEndUpSt'] + 0.15
        sw_pres = mask[np.logical_and(time_arr>= r1[0], time_arr<=r1[1])].sum()

        if sw_pres > 1:
            states.loc[i, 'has_sw'] = True


    # --- FIND IS SLEEP STATES HAVE SLOW WAVES IN THEM ---------

    scoring['has_sw'] = False
    for i, row in scoring.iterrows():

        r1 = row['t1'], row['t2']
        sw_pres = mask[np.logical_and(time_arr>= r1[0], time_arr<=r1[1])].sum()

        if sw_pres > 1 and row['scoring'] == 'SWS':

            scoring.loc[i, 'scoring_with_sw'] = 'validated_SWS'
        else:
            scoring.loc[i, 'scoring_with_sw'] = scoring.loc[i, 'scoring']



    # --- FIND IF UP STATES OCCUR IN A SW PERIOD WITH A SW ----------
    states['is_in_SWS'] = False
    for i, row in states.iterrows():

        r1 = row['TimeStartUpSt'], row['TimeEndUpSt']

        tdiff = r1[0] - scoring['t1']
        idx = tdiff[tdiff>0].argmin()
        sleep_st = scoring.iloc[idx]['scoring_with_sw']
        states.loc[i, 'sleep_state'] = sleep_st

        if sleep_st == 'validated_SWS':
            states.loc[i, 'is_in_SWS'] = True



    # f, ax = plt.subplots(1, 3, figsize=[7, 4])
    #
    # total_sess_duration = time_arr[-1] - time_arr[0]
    # scoring_duration = scoring['t2'].max() - scoring['t1'].min()
    # ax[0].bar(x=[0, 1], height=[total_sess_duration, scoring_duration])
    # ax[0].set_xticks([0, 1])
    # ax[0].set_xticklabels(['Total\ntime', 'Sleep-scored\ntime'])
    #
    # sc_group = scoring.groupby('scoring').sum().reset_index()
    # sns.barplot(data=sc_group, y='d', x='scoring', ax=ax[1])
    #
    # sc_group = scoring.groupby('scoring_with_sw').sum().reset_index()
    # sns.barplot(data=sc_group, y='d', x='scoring_with_sw', ax=ax[2])
    #
    # for axx in ax:
    #     axx.set_xticklabels(axx.get_xticklabels(), rotation=90)
    #     axx.set_xlabel('')
    #     axx.set_ylabel('Total duration')
    # sns.despine()
    # plt.tight_layout()
    scoring_palette = {'SWS' : sns.xkcd_rgb['red'],
                       'validated_SWS' : sns.xkcd_rgb['orange'],
                        'AWAKE' : sns.xkcd_rgb['blue'],
                       'REM' : sns.xkcd_rgb['brown'],
                       'UNKNOWN' : sns.xkcd_rgb['grey'],
                       }
    order = [k for k in scoring_palette.keys()]



    f, ax = plt.subplots(1, 5, figsize=[12, 5])

    sc_group = scoring.groupby('scoring').sum().reset_index()
    sns.barplot(data=sc_group, y='d', x='scoring', ax=ax[0],
                palette=scoring_palette, order=order)
    ax[0].set_ylabel('Total duration [s] in original scoring')
    ax[0].set_title('Original sleep scoring')


    sc_group = scoring.groupby('scoring_with_sw').sum().reset_index()
    sns.barplot(data=sc_group, y='d', x='scoring_with_sw', ax=ax[1],
                palette=scoring_palette, order=order)
    ax[1].set_ylabel('Total duration [s] in validated scoring')
    ax[1].set_title('Validated sleep scoring')


    sns.countplot(data=states, x='sleep_state', ax=ax[2],
                  palette=scoring_palette, order=order)
    ax[2].set_ylabel('# of UP states')
    ax[2].set_title('UP states in\nvalidated sleep scoring')


    sns.countplot(data=states, x='is_in_SWS', ax=ax[3], order=[False, True])
    ax[3].set_xticklabels(['Not in\nvalidated SWS', 'In validated SWS'])
    ax[3].set_ylabel('# of UP states')
    ax[3].set_title('UP states\nin validated SWS')

    sns.countplot(data=states, x='has_sw', ax=ax[4], order=[False, True])
    ax[4].set_xticklabels(['Without\nslow wave', 'With\nslow wave'])
    ax[4].set_xlabel('')
    ax[4].set_ylabel('# of UP states')
    ax[4].set_title('UP states\nwith slow wave')

    for axx in ax:
        axx.set_xlabel('')
        axx.set_xticklabels(axx.get_xticklabels(), rotation=90)

    sns.despine()
    plt.tight_layout()

    f.savefig(os.path.join(plot_folder, '{}_overview_UP_states.{}'.format(session_id, plot_format)), dpi=400)



    tp = np.arange(np.ceil(time_arr.min() / 1000), np.floor(time_arr.max() / 1000), 2)

    views = [(int(t*1000), int(t*1000 + 200)) for t in tp]

    for vv, (t_start, t_stop) in enumerate(views):

        indx = np.where(np.logical_and(time_arr>t_start, time_arr<t_stop))

        import matplotlib as mpl

        mpl.rcParams['agg.path.chunksize'] = 10000

        f, ax = plt.subplots(1, 1, figsize=(16, 4.5))


        ax.plot(time_arr[indx], data_filt[indx], 'k', label='Bandpass filtered LFP')
        ax.plot(time_arr[indx], data[indx], 'k', alpha=0.2, label='LFP')
        ax.plot(time_arr[indx], sw_highlight[indx], 'indianred')

        start_time_indx = np.where(np.diff(mask) == 1)[0]
        start_time_indx = start_time_indx[np.logical_and(time_arr[start_time_indx] >= t_start,
                                                         time_arr[start_time_indx] <= t_stop)]
        #ax.plot(time_arr[start_time_indx], data_filt[start_time_indx], 'bo', label='Negative peaks')

        for i, row in states.iterrows():
            if row['TimeStartUpSt'] >= t_start and row['TimeEndUpSt'] <= t_stop:
                print(row)
                if row['has_sw']:
                    ax.axvspan(row['TimeStartUpSt'], row['TimeEndUpSt'],
                               facecolor=sns.xkcd_rgb['light orange'],
                               edgecolor=sns.xkcd_rgb['dark orange'],
                               alpha=0.6,lw=0, zorder=-10)
                    #ax.axvline(row['StartUpStates'], lw=1, c=sns.xkcd_rgb['dark orange'], zorder=-10)
                else:
                    ax.axvspan(row['TimeStartUpSt'], row['TimeEndUpSt'],
                               facecolor=sns.xkcd_rgb['light yellow'],
                               edgecolor=sns.xkcd_rgb['dark yellow'],
                               alpha=0.9, lw=0, zorder=-10)
                    #ax.axvline(row['StartUpStates'], lw=1, c=sns.xkcd_rgb['dark yellow'], zorder=-10)

                #print(row['StartUpStates'], row['EndUpstates'])

        for i, p in scoring.iterrows():
            where = ax.get_ylim()[0]+(ax.get_ylim()[1] - ax.get_ylim()[0])/10
            ax.plot((p['t1'], p['t2']), (where, where), c=scoring_palette[p['scoring']], lw=5)

        ax.set_xlabel('Time (seconds)')
        ax.set_ylabel('Amplitude (uV)')
        #ax.set_ylim([-400, 400])
        ax.set_xlim([t_start, t_stop])

        #ax.xlim([0, times[-1]])
        #ax.set_title('LFP data (filtered) - session {}'.format(session_id))
        ax.legend().remove()
        sns.despine()

        f.savefig(os.path.join(plot_folder, '{}_slow_waves_{}.{}'.format(session_id, vv, plot_format)), dpi=400)


    # --- COMPUTE SPECTRAL DENSITY ---
    win = int(4 * sf)  # Window size is set to 4 seconds
    freqs, psd = welch(arr, sf, nperseg=win,
                       average='median')  # Works with single or multi-channel data

    print(freqs.shape, psd.shape)  # psd has shape (n_channels, n_frequencies)

    # Plot
    f, ax = plt.subplots(1, 1, figsize=[5, 5])

    ax.plot(freqs, psd, 'k', lw=2)
    ax.fill_between(freqs, psd, cmap='Spectral')
    ax.set_xlim(0, 50)
    ax.set_yscale('log')
    sns.despine()
    ax.set_title(channel_name)
    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel('PSD log($uV^2$/Hz)')
    plt.tight_layout()



    #f = plt.figure()
    #
    # ax = sw.plot_average(time_before=0.4, time_after=0.8, center="NegPeak",
    #                 ci='sd', figsize=(3, 3))
    # ax.legend().remove()
    # plt.tight_layout()
    # sns.despine()

    # --- PERCENTAGE RETAINED UP STATES -------

    f, ax = plt.subplots(1, 2, figsize=[5, 5])
    sns.countplot(data=states, x='has_sw', ax=ax[0])
    ax[0].set_xticklabels(['Without SW', 'With SW'])
    ax[0].set_xlabel('')
    ax[0].set_ylabel('# of UP states')
    ax[0].set_title('Slow wave presence')

    states_duration = states['TimeEndUpSt'] - states['TimeStartUpSt']
    sns.distplot(states_duration, ax=ax[1], norm_hist=True)
    ax[1].set_xlabel('Time [s]')
    ax[1].set_ylabel('Density')
    ax[1].set_title('Up states duration')

    sns.despine()
    plt.tight_layout()


    f.savefig(os.path.join(plot_folder, '{}_ratio_up_states_with_sw.{}'.format(session_id, plot_format)), dpi=400)


# --- LEGEND 1
f, ax = plt.subplots(1, 1, figsize=(2, 2))

custom_lines = [Line2D([0], [0], color=scoring_palette['SWS'], lw=5),
                Line2D([0], [0], color=scoring_palette['REM'], lw=5),
                Line2D([0], [0], color=scoring_palette['AWAKE'], lw=5),
                Line2D([0], [0], color=scoring_palette['UNKNOWN'], lw=5)]
texts = ['SWS', 'Rem', 'Awake', 'Unknown']

ax.legend(custom_lines, texts, loc='center', frameon=False)
ax.axis('off')
f.savefig(os.path.join(plot_folder, 'leg1.{}'.format(plot_format)), dpi=400)


# --- LEGEND 2
f, ax = plt.subplots(1, 1, figsize=(2, 2))

custom_lines = [Line2D([0], [0], color='k', lw=1),
                Line2D([0], [0], color='grey', lw=1),
                Line2D([0], [0], color='indianred', lw=1)]

texts = ['Filtered LFP', 'LFP', 'Detected slow wave']

ax.legend(custom_lines, texts, loc='center', frameon=False)
ax.axis('off')
f.savefig(os.path.join(plot_folder, 'leg2.{}'.format(plot_format)), dpi=400)


# --- LEGEND 3
f, ax = plt.subplots(1, 1, figsize=(2, 2))

custom_lines = [Line2D([0], [0], color=sns.xkcd_rgb['light orange'], alpha=0.5, lw=8),
                Line2D([0], [0], color=sns.xkcd_rgb['light yellow'], alpha=0.5, lw=8)]

texts = ['UP states with SW', 'UP states without SW']

ax.legend(custom_lines, texts, loc='center', frameon=False)
ax.axis('off')
f.savefig(os.path.join(plot_folder, 'leg3.{}'.format(plot_format)), dpi=400)





# f.add_axes(ax)
# f.savefig(os.path.join(PLOTS_PATH, 'sw_average.{}'.format(plot_format)), dpi=400)
#


"""
- which channel to pick
- look for slow waves within the bound?
- separate sleep?
- up states terminate a while before the end of the recording
- validate in one session or in all sessions?

"""









